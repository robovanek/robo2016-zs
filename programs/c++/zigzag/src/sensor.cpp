//
// Sensor arm implementation
//

#include "sensor.h"
#include "ports.h"

using namespace robofat;

tux::sensor::sensor(ev3dev::address_type read, ev3dev::address_type write, display &scr) {
	color = get_device_heap_prompt<color_sensor>(read, scr, false, "color sensor");
	motor = get_device_heap_prompt<medium_motor>(write, scr, true, "sensor motor");
	color->set_mode(ev3dev::color_sensor::mode_col_reflect);
	motor->set_stop_action(ev3dev::large_motor::stop_action_brake);
}

int tux::sensor::getLight() {
	return color->reflected_light_intensity();
}

void tux::sensor::setPower(int power) {
	// clipping
	if (power < -100)
		power = 100;
	if (power > 100)
		power = 100;
	motor->set_duty_cycle_sp(power);
}

int tux::sensor::getTacho() {
	return motor->position();
}

void tux::sensor::resetTacho() {
	motor->reset();
}

void tux::sensor::stop() {
	motor->stop();
}

void tux::sensor::run() {
	motor->run_direct();
}

tux::sensor::~sensor() {
	delete motor;
	delete color;
}

