//
// User interaction methods implementation
//

#include <ev3dev.h>
#include "ui.h"

using namespace std;
using namespace ev3dev;

tux::trackmode tux::ui::getTrackMode() {
	int index = disp.show_menu("Track mode?", {"left", "right"}, 0, false);
	if (index == 0)
		return trackmode::left;
	else if (index == 1)
		return trackmode::right;
	return trackmode::right;
}

bool tux::ui::waitStart() {
	int index = disp.show_menu("START", {"Yes!", "No."}, 0, true);
	bool start = index == 0;
	if (start)
		disp.print_centered("-> START", display::attrib::attr_bold);
	else
		disp.print_centered("-> EXIT", display::attrib::attr_bold);
	return start;
}

void tux::ui::waitExit() {
	display::keys value;
	do {
		value = disp.getkey();
	} while (value != display::keys::key_escape);
}

tux::ui::ui(display &disp) : disp(disp) {

}







