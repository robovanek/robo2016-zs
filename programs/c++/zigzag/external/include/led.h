/*
 * RoboFAT, a small C++ robotic library for ev3dev
 * Copyright (C) 2016  Jakub Vaněk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (A bit useless) LED abstraction of ev3dev led subsystem.
 */

#ifndef ROBOFAT_LED_H
#define ROBOFAT_LED_H

#include "ev3dev.h"

namespace robofat {
	/**
	 * \brief Combined LED light color.
	 */
	enum led_color {
		/**
		 * \brief Black light colour (i.e. no color).
		 */
				BLACK = 0,
		/**
		 * \brief Red light colour (red full, green off).
		 */
				RED = 1,
		/**
		 * \brief Green light colour (red off, green full).
		 */
				GREEN = 2,
		/**
		 * \brief Amber (dark orange) light colour (red strong, green weak).
		 */
				AMBER = 3,
		/**
		 * \brief Orange light colour (red strong, green strong).
		 */
				ORANGE = 4,
		/**
		 * \brief Yellow light colour (red weak, green strong).
		 */
				YELLOW = 5
	};

	/**
	 * \brief Turn the LEDs off.
	 */
	extern void led_off();

	/**
	 * \brief Set LED color
	 *
	 * @param left      Left LED color
	 * @param right     Right LED color
	 */
	extern void led_set(led_color left, led_color right);
}

#endif //ROBOFAT_LED_H
