#!/bin/sh
cd "$(dirname "$(realpath "$0")")/..";
export PATH=/home/kuba/cmake3.5/bin:$PATH
cmake -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_TOOLCHAIN_FILE=/home/kuba/x-tools/ev3dev-toolchain.cmake \
      -DCURSES_LIBRARY=/home/kuba/ncurses-6.0/usr/lib/libncurses.a \
      -DCURSES_INCLUDE_PATH=/home/kuba/ncurses-6.0/usr/include .
make -j2
#cp ../out/artifact/Brick_JAR/TARDIS.jar ../out/
