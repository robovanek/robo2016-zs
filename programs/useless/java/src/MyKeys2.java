import lejos.hardware.Sound;
import lejos.internal.io.NativeDevice;
import lejos.internal.io.SystemSettings;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

import java.nio.ByteBuffer;

/**
 * Demolished version of EV3Keys
 */
public class MyKeys2 {
    public static final int ID_UP     = 0x1;  // 00000001
    public static final int ID_ENTER  = 0x2;  // 00000010
    public static final int ID_DOWN   = 0x4;  // 00000100
    public static final int ID_RIGHT  = 0x8;  // 00001000
    public static final int ID_LEFT   = 0x10; // 00010000
    public static final int ID_ESCAPE = 0x20; // 00100000
    public static final int ID_TOUCH  = 0x40; // 01000000
    public static final int ID_ALL    = 0x7f; // 01111111

    public static final String VOL_SETTING = "lejos.keyclick_volume";
    public static final String LEN_SETTING = "lejos.keyclick_length";
    public static final String FREQ_SETTING = "lejos.keyclick_frequency";
    private static final int DEBOUNCE_TIME = 10;
    private static final int POLL_TIME = 50;
    private SampleProvider hahaha;

    // protected by monitor
    private int clickVol;
    private int clickLen;
    private int clickFreq = 1000;
    private int curButtonsS;
    // not protected by any monitor
    private int curButtonsE;

    private ByteBuffer buttonState;

    public MyKeys2(SampleProvider prov) {
        clickVol = SystemSettings.getIntSetting(VOL_SETTING, 20);
        clickLen = SystemSettings.getIntSetting(LEN_SETTING, 50);
        clickFreq = SystemSettings.getIntSetting(FREQ_SETTING, 1000);
        NativeDevice dev = new NativeDevice("/dev/lms_ui");
        buttonState = dev.mmap(6).getByteBuffer(0, 6);
        hahaha = prov;
        curButtonsE = curButtonsS = getButtons();
    }

    public int waitForAnyPress(int timeout) {
        long end = (timeout == 0 ? 0x7fffffffffffffffL : System
                .currentTimeMillis() + timeout);
        try {
            int oldDown = curButtonsE;
            while (true) {
                long curTime = System.currentTimeMillis();
                if (curTime >= end)
                    return 0;
                Thread.sleep(POLL_TIME);

                int newDown = curButtonsE = readButtons();
                int pressed = newDown & (~oldDown);
                if (pressed != 0)
                    return pressed;
                oldDown = newDown;
            }
        } catch (InterruptedException e) {
            // TODO: Need to decide how to handle this properly
            // preserve state of interrupt flag
            Thread.currentThread().interrupt();
            return 0;
        }
    }

    public int waitForAnyPress() {
        return waitForAnyPress(0);
    }

    public int getButtons() {
        // read buttons and de-bounce them
        int state1, state2 = 0;
        for (; ; ) {
            state1 = checkButtons() | checkTouch();
            if (state1 == state2)
                return state1;
            Delay.msDelay(DEBOUNCE_TIME);
            state2 = checkButtons() | checkTouch();
        }
    }

    public int readButtons() {
        int newButtons = getButtons();

        int pressed = newButtons & (~curButtonsS);
        curButtonsS = newButtons;
        if (pressed != 0 && clickVol != 0) {
            int tone = clickFreq;
            if (tone != 0)
                Sound.playTone(tone, clickLen, -clickVol);
        }
        return newButtons;
    }

    private int checkButtons() {
        int state = 0;
        for (int i = 0; i < buttonState.capacity(); i++)
            if (buttonState.get(i) != 0)
                state |= 1 << i;
        return state;
    }

    private int checkTouch() {
        float[] sample = new float[hahaha.sampleSize()];
        hahaha.fetchSample(sample, 0);
        return sample[0] == 1.0f ? ID_TOUCH : 0;
    }
}




