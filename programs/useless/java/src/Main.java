// importy balíčků

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.LED;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.RegulatedMotor;

/**
 * Hlavní třída
 */
public class Main implements Runnable {
    /**
     * index řádky s informacemi
     */
    private static final int INFO_LINE = 6;
    /**
     * vstup přes tlačítka
     */
    private static MyKeys2 keys;
    /**
     * levý motor
     */
    private RegulatedMotor left;
    /**
     * pravý motor
     */
    private RegulatedMotor right;
    /**
     * světýlko na kostce
     */
    private LED led;
    /**
     * displej
     */
    private TextLCD lcd;

    /**
     * konstruktor
     */
    private Main() {
        // získej tuto kostku
        Brick brick = BrickFinder.getLocal();
        // inicializuj motory na portech B a C
        left = new EV3LargeRegulatedMotor(brick.getPort("B"));
        right = new EV3LargeRegulatedMotor(brick.getPort("C"));
        // nastav synchronizaci
        left.synchronizeWith(new RegulatedMotor[]{right});
        // nastav rychlost
        left.setSpeed(540);
        right.setSpeed(540);
        // získej světýlko
        led = brick.getLED();
        // initializuj tlačítko na portu S1 + input
        keys = new MyKeys2(new EV3TouchSensor(brick.getPort("S1")).getTouchMode());
        // získej textové rozhranní na displeji
        lcd = brick.getTextLCD(Font.getDefaultFont());
        // vypni automatické obnovení
        lcd.setAutoRefresh(false);
    }

    /**
     * hlavní funkce
     *
     * @param args ignore
     */
    public static void main(String[] args) {
        // vytvoř a spusť instanci hlavní třídy
        new Main().run();
    }

    /**
     * algoritmus programu
     */
    @Override
    public void run() {
        //
        print();
        Mode mode = Mode.STOP;
        boolean cont = true;
        while (cont) {
            setMode(mode);

            int data = keys.waitForAnyPress();
            int press = data & MyKeys2.ID_ALL;
            if ((press & MyKeys2.ID_ESCAPE) != 0) {
                mode = Mode.STOP;
                cont = false;
            } else if ((press & (MyKeys2.ID_ENTER | MyKeys2.ID_TOUCH)) != 0) {
                mode = Mode.STOP;
            } else if ((press & MyKeys2.ID_UP) != 0) {
                mode = Mode.FORWARD;
            } else if ((press & MyKeys2.ID_DOWN) != 0) {
                mode = Mode.BACKWARD;
            } else if ((press & MyKeys2.ID_RIGHT) != 0) {
                mode = Mode.RIGHT;
            } else if ((press & MyKeys2.ID_LEFT) != 0) {
                mode = Mode.LEFT;
            }
        }
    }
    private void setMode(Mode m) {
        switch (m) {
            case STOP:
                led.setPattern(1);
                left.startSynchronization();
                left.flt(true);
                right.flt(true);
                left.endSynchronization();
                info("stop");
                break;
            case FORWARD:
                led.setPattern(4);
                left.startSynchronization();
                left.forward();
                right.forward();
                left.endSynchronization();
                info("dopredu");
                break;
            case BACKWARD:
                led.setPattern(5);
                left.startSynchronization();
                left.backward();
                right.backward();
                left.endSynchronization();
                info("dozadu");
                break;
            case LEFT:
                led.setPattern(6);
                left.startSynchronization();
                left.backward();
                right.forward();
                left.endSynchronization();
                info("doleva");
                break;
            case RIGHT:
                led.setPattern(6);
                left.startSynchronization();
                left.forward();
                right.backward();
                left.endSynchronization();
                info("doprava");
                break;
        }
    }

    private void info(String str) {
        lcd.clear(INFO_LINE);
        lcd.drawString("Mode = " + str, 0, INFO_LINE);
        lcd.refresh();
    }

    private void print() {
        lcd.clear();
        lcd.drawString("ENTER:  stop", 0, 0);
        lcd.drawString("UP:     dopredu", 0, 1);
        lcd.drawString("DOWN:   dozadu", 0, 2);
        lcd.drawString("LEFT:   doleva", 0, 3);
        lcd.drawString("RIGHT:  doprava", 0, 4);
        lcd.drawString("ESCAPE: exit", 0, 5);
        lcd.drawString("Mod = none", 0, INFO_LINE);
        lcd.refresh();
    }

    private enum Mode {
        STOP, FORWARD, BACKWARD, LEFT, RIGHT
    }
}
