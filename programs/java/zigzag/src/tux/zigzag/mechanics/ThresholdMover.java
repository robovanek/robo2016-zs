package tux.zigzag.mechanics;

import lejos.robotics.chassis.Chassis;
import lejos.utility.Delay;
import tux.shared.AdvancedRunnable;
import tux.shared.TrackMode;
import tux.shared.sensor.SensorConstraints;
import tux.shared.utils.Settings;
import tux.shared.utils.Utils;
import tux.zigzag.Main;
import tux.zigzag.sensor.Direction;
import tux.zigzag.sensor.Pack;
import tux.zigzag.sensor.Sample;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Moves the zigzag by finding first value below some threshold in the correct direction.
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public class ThresholdMover implements AdvancedRunnable {
    /**
     * TARDIS core for controlling time and space flow
     */
    private final Chassis chassis;
    //private final ExperimentalTARDIS tardis;
    /**
     * Input sample packets
     */
    private final BlockingQueue<Pack> input;
    /**
     * {@link tux.zigzag.sensor.SensorReader} instance for controlling scanning range
     */
    private final SensorConstraints limits;
    private final boolean linear;
    /**
     * Associated thread for {@link #getThread()}
     */
    private Thread threadInstance = null;
    /**
     * Run flag
     */
    private volatile boolean run = true;
    /**
     * Detection threshold. This is maximum value for detection, all lesser values are counted as black.
     */
    private int threshold;
    private int integral = 0;
    private int lastError = 0;

    /**
     * Constructs new mover
     *
     * @param ch    Chassis used for moving
     * @param input Input queue from packer (or other) thread
     */
    public ThresholdMover(Chassis ch, BlockingQueue<Pack> input, SensorConstraints sensor) {
        this.chassis = ch;
        this.input = input;
        this.limits = sensor;
        linear = Settings.EFFECT_ANGLE_KP != 0.0;
        setThreshold(Settings.ANALYZE_THRESHOLD);
    }

    @Override
    public Thread getThread() {
        if (threadInstance == null) {
            threadInstance = new Thread(this);
            threadInstance.setPriority(Thread.NORM_PRIORITY);
            threadInstance.setDaemon(false);
            threadInstance.setName("brain");
        }
        return threadInstance;
    }

    @Override
    public void setStopFlag(boolean flag) {
        run = !flag;
        if (threadInstance != null && flag)
            threadInstance.interrupt();
    }


    private int searchForward(List<Sample> p) {
        int size = p.size();
        for (int i = 0; i < size; i++) {
            if (p.get(i).getValue() < threshold) {
                return p.get(i).getAngle();
            }
        }
        return Integer.MIN_VALUE;
    }

    private int searchBackward(List<Sample> p) {
        int size = p.size();
        for (int i = size - 1; i >= 0; i--) {
            if (p.get(i).getValue() < threshold) {
                return p.get(i).getAngle();
            }
        }
        return Integer.MIN_VALUE;
    }

    @Override
    public void run() {
        int lastAngle = Settings.EFFECT_SENSOR_OFFSET;
        long lastTimestamp = 0;
        boolean first = true;
        while (run) {
            if (first) {
                first = false;
                lastTimestamp = System.nanoTime();
            } else {
                sleep(lastTimestamp);
                lastTimestamp = System.nanoTime();
            }
            // fetch packet
            Pack p = Utils.take(input);
            if (p == null)
                continue;
            // search for black line
            int angle;
            if (Main.TRACK_MODE == TrackMode.LEFT) {
                if (p.getDirection() == Direction.LEFT_TO_RIGHT) {
                    angle = searchForward(p.getSamples());
                } else {
                    angle = searchBackward(p.getSamples());
                }
            } else /*if (Main.TRACK_MODE == TrackMode.RIGHT)*/ {
                if (p.getDirection() == Direction.LEFT_TO_RIGHT) {
                    angle = searchBackward(p.getSamples());
                } else {
                    angle = searchForward(p.getSamples());
                }
            }
            boolean found = angle != Integer.MIN_VALUE;
            // if we haven't found anything
            if (!found) {
                // set destination to the last angle
                angle = lastAngle;
                if (lastAngle > 0)
                    angle += Settings.EFFECT_EXTREME;
                else if (lastAngle < 0)
                    angle -= Settings.EFFECT_EXTREME;
                //angle = Utils.limit(angle, Settings.SENSOR_RIGHT_LIMIT, Settings.SENSOR_LEFT_LIMIT);
            } else {
                // determine angle from center
                angle -= Settings.EFFECT_SENSOR_OFFSET;
                // set last angle to destination
                lastAngle = angle;
            }
            // set new range
            setRange(angle, found);
            // affect the zigzag
            effect(angle);
        }
        chassis.stop();
    }

    /**
     * Sleep to create more consistent timing
     *
     * @param lastTimestamp Last timestamp
     */
    private boolean sleep(long lastTimestamp) {
        long wait = System.nanoTime() - lastTimestamp;
        Delay.msDelay(Settings.EFFECT_SLEEP - wait / 1000000);
        return !Thread.currentThread().isInterrupted();
    }

    /**
     * Set sensor scanning range from angle offset
     *
     * @param angle Actual angle offset
     */
    private void setRange(int angle, boolean found) {
        int sensor_angle = angle + Settings.EFFECT_SENSOR_OFFSET;
        if (found) {
            int lLimit = Utils.limit(sensor_angle + Settings.EFFECT_SENSOR_DYNAMIC_RANGE,
                    Settings.SENSOR_RIGHT_LIMIT, Settings.SENSOR_LEFT_LIMIT);
            int rLimit = Utils.limit(sensor_angle - Settings.EFFECT_SENSOR_DYNAMIC_RANGE,
                    Settings.SENSOR_RIGHT_LIMIT, Settings.SENSOR_LEFT_LIMIT);
            limits.setLeftLimit(lLimit);
            limits.setRightLimit(rLimit);
            limits.setPower(Settings.SENSOR_POWER);
        } else {
            int lLimit, rLimit;
            if (angle > 0) {
                lLimit = Settings.SENSOR_LEFT_LIMIT;
                rLimit = Settings.EFFECT_SENSOR_OFFSET;
            } else if (angle < 0) {
                lLimit = Settings.EFFECT_SENSOR_OFFSET;
                rLimit = Settings.SENSOR_RIGHT_LIMIT;
            } else {
                lLimit = Settings.EFFECT_SENSOR_OFFSET + Settings.EFFECT_SENSOR_DYNAMIC_RANGE;
                rLimit = Settings.EFFECT_SENSOR_OFFSET - Settings.EFFECT_SENSOR_DYNAMIC_RANGE;
            }
            limits.setLeftLimit(lLimit);
            limits.setRightLimit(rLimit);
            limits.setPower(Settings.SENSOR_POWER + Settings.EFFECT_SENSOR_POWER_INCREASE);
        }
    }

    /**
     * Modify the velocity of the zigzag.
     *
     * @param angle Turning angle
     */
    private void effect(int angle) {
        integral += angle;
        int diff = angle - lastError;
        lastError = angle;
        float angularVelocity = (float) (
                (linear ? Settings.EFFECT_ANGLE_KP : Settings.EFFECT_ANGLE_KQ) * angle +
                        Settings.EFFECT_ANGLE_KI * integral +
                        Settings.EFFECT_ANGLE_KD * diff);
        float change = Math.abs(angularVelocity);
        if (change < Settings.EFFECT_SLOWDOWN_THRESHOLD)
            change = 0;
        float linearVelocity = (float) Math.max(
                Settings.WHEEL_LINEAR_SPEED - change * Settings.EFFECT_LINEAR_KP,
                Settings.ROBOT_MIN_SPEED);

        chassis.setVelocity(linearVelocity, angularVelocity);
    }

    /**
     * Get detection threshold
     *
     * @return Integer in range 0-100
     */
    public int getThreshold() {
        return threshold;
    }

    /**
     * Set detection threshold
     *
     * @param threshold Integer in range 0-100
     */
    private void setThreshold(int threshold) {
        if (100 >= threshold && threshold >= 0)
            this.threshold = threshold;
        else
            throw new IllegalArgumentException("Threshold must be in range 0-100!");
    }
}
