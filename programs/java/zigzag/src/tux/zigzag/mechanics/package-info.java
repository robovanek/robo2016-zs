/**
 * Classes for moving the zigzag.
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.4
 */
package tux.zigzag.mechanics;