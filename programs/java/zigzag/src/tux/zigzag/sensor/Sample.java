package tux.zigzag.sensor;

/**
 * Set of three quantities<br/>
 * - time (as nanosecond timestamp)
 * - angle of motor (in degrees)
 * - reflected light (in percent)
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.1
 */
public class Sample {
    /**
     * {@link System#nanoTime()} at the moment when this sample was taken
     * Units: ns
     */
    private final long timestamp;
    /**
     * Reflected light
     * Units: %
     * Range: 0-100
     */
    private final int value;
    /**
     * Sensor azimuth at the moment when this sample was taken
     * Azimuth 0 = the leftmost configured position of the sensor at the moment when this sample was taken
     * Units: ° (degrees)
     * Range: 0 - angularSize at the moment when this sample was taken
     */
    private final int angle;

    /**
     * Construct a sample
     *
     * @param timestamp Precise timestamp of the moment when the sample was taken
     * @param value     Value of the sample
     * @param angle     Sensor azimuth at the moment when the sample was taken
     */
    public Sample(long timestamp, int value, int angle) {
        this.timestamp = timestamp;
        this.value = value;
        this.angle = angle;
    }

    /**
     * {@link System#nanoTime()} at the moment when this sample was taken
     * Units: ns
     *
     * @return time
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Reflected light
     * Units: %
     * Range: 0-100
     *
     * @return reflected light
     */
    public int getValue() {
        return value;
    }

    /**
     * Sensor azimuth at the moment when this sample was taken
     * Azimuth 0 = the leftmost configured position of the sensor at the moment when this sample was taken
     * Units: ° (degrees)
     * Range: 0 - angularSize at the moment when this sample was taken
     *
     * @return azimuth
     */
    public int getAngle() {
        return angle;
    }

}
