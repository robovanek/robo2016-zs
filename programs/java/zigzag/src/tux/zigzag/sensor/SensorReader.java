package tux.zigzag.sensor;

import lejos.robotics.Encoder;
import lejos.utility.Delay;
import tux.shared.sensor.HackedSensor;
import tux.shared.utils.Settings;

import java.io.Closeable;
import java.io.IOException;

/**
 * Sensor reader
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public class SensorReader implements Closeable {
    /**
     * Encoder used for obtaining the angle
     */
    private final Encoder m;
    /**
     * Sensor used for obtaining the reflected light percentage
     */
    private final HackedSensor s;

    /**
     * Constructs new sensor reader.
     *
     * @param s     EV3 color sensor for reflectiveness input
     * @param motor Motor for angle input
     */
    public SensorReader(HackedSensor s, Encoder motor) {
        this.s = s;
        this.m = motor;
    }

    /**
     * Read the sample from motor and sensor
     *
     * @return Sample
     */
    public Sample fetchSample() {
        long timestamp = System.nanoTime();
        int value = s.getRedFast();
        int angle = m.getTachoCount();
        return new Sample(timestamp, value, angle);
    }

    /**
     * Wait until next data are available
     *
     * @param sample Last sample
     * @return If the wait was successful
     */
    public boolean sleep(Sample sample) {
        if (sample == null)
            return true;
        long wait = System.nanoTime() - sample.getTimestamp();
        Delay.msDelay(Settings.SENSOR_SLEEP - wait / 1000000);
        return !Thread.currentThread().isInterrupted();
    }

    @Override
    public void close() throws IOException {
        s.close();
    }
}
