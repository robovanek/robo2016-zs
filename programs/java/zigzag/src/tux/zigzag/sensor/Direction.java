package tux.zigzag.sensor;

/**
 * Direction of samples of the packet
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.1
 */
public enum Direction {
    /**
     * Samples are progressing from index 0 to end from right to left.
     */
    RIGHT_TO_LEFT,
    /**
     * Samples are progressing from index 0 to end from left to right.
     */
    LEFT_TO_RIGHT
}
