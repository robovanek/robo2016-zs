package tux.alter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by kuba on 9.4.16.
 */
public final class Log {
    private static Log instance;
    private ArrayList<Sample> data;

    private Log() {
        data = new ArrayList<>(30000);
    }

    public static Log getInstance() {
        if (instance == null)
            instance = new Log();
        return instance;
    }

    public void add(Sample s) {
        data.add(s);
    }

    public void write() {
        NumberFormat format = NumberFormat.getInstance(Locale.forLanguageTag("cs"));
        long startTime = data.get(0).time;
        StringBuilder build = new StringBuilder();
        build.append("time;azimuth;lightError;targAz;azError;ccw;sp;sd;mp;md\n");
        try (FileWriter str = new FileWriter("/home/lejos/programs/datalog3.csv");
             BufferedWriter write = new BufferedWriter(str)) {
            for (Sample s : data) {
                build.append(Math.round((s.time - startTime) / 1_000_000f)).append(';');
                build.append(s.azimuth).append(';');

                build.append(s.lightError).append(';');
                build.append(s.targetAzimuth).append(';');
                build.append(s.azimuthError).append(';');

                build.append(s.robot_ccw).append(';');

                build.append(format.format(s.s_p)).append(';');
                build.append(format.format(s.s_d)).append(';');
                build.append(format.format(s.m_p)).append(';');
                build.append(format.format(s.m_d)).append('\n');

                write.write(build.toString());
                build.setLength(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Sample {
        public long time;
        public int azimuth;

        public int lightError;
        public int targetAzimuth;
        public int azimuthError;

        public int robot_ccw;

        public float s_p;
        public float s_d;
        public float m_p;
        public float m_d;
    }
}
