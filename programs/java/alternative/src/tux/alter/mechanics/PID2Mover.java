package tux.alter.mechanics;

import lejos.hardware.motor.UnregulatedMotor;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
import tux.alter.Log;
import tux.alter.Main;
import tux.shared.AdvancedRunnable;
import tux.shared.TrackMode;
import tux.shared.pid.DifferentialController;
import tux.shared.pid.PID;
import tux.shared.pid.filter.Filter;
import tux.shared.pid.filter.Filters;
import tux.shared.sensor.HackedSensor;
import tux.shared.utils.Settings;

/**
 * PID based mover
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.6
 */
public class PID2Mover implements AdvancedRunnable {
    private final HackedSensor sensor;
    private Thread thread = null;
    private volatile boolean run = true;
    private PID pidSensor;
    private PID pidWheels;
    private DifferentialController control;
    private UnregulatedMotor sensorMotor;
    private Filter transferFilter;
    private int sensorMin;
    private int sensorMax;
    private Filter errorFilter;

    public PID2Mover(RegulatedMotor lMotor, RegulatedMotor rMotor, UnregulatedMotor sMotor, HackedSensor sSensor) {
        lMotor.synchronizeWith(new RegulatedMotor[]{rMotor});
        this.sensor = sSensor;

        this.pidSensor = new PID(Settings.PID2_SENSOR_KP,
                Settings.PID2_SENSOR_KI,
                Settings.PID2_SENSOR_KD,
                Filters.getNewFilter(Filters.getMode(Settings.PID2_SENSOR_FILTER_TYPE),
                        Settings.PID2_SENSOR_FILTER_ARG1,
                        Settings.PID2_SENSOR_FILTER_ARG2,
                        (int) Settings.PID2_SENSOR_FILTER_ARG1,
                        Settings.PID2_SENSOR_FILTER_ARG1
                ));
        this.pidWheels = new PID(Settings.PID2_WHEELS_KP,
                Settings.PID2_WHEELS_KI,
                Settings.PID2_WHEELS_KD,
                Filters.getNewFilter(Filters.getMode(Settings.PID2_WHEELS_FILTER_TYPE),
                        Settings.PID2_WHEELS_FILTER_ARG1,
                        Settings.PID2_WHEELS_FILTER_ARG2,
                        (int) Settings.PID2_WHEELS_FILTER_ARG1,
                        Settings.PID2_WHEELS_FILTER_ARG1
                ));
        this.control = new DifferentialController(lMotor, rMotor,
                Settings.WHEEL_LEFT_INVERT,
                Settings.WHEEL_RIGHT_INVERT);
        this.errorFilter = Filters.getNewFilter(Filters.getMode(Settings.PID2_SLOWDOWN_FILTER_TYPE),
                Settings.PID2_SLOWDOWN_FILTER_ARG1,
                Settings.PID2_SLOWDOWN_FILTER_ARG2,
                (int) Settings.PID2_SLOWDOWN_FILTER_ARG1,
                Settings.PID2_SLOWDOWN_FILTER_ARG1);
        control.setDefaultSpeed(Settings.PID2_WHEELS_SPEED);
        control.setSlowdownLimit(Settings.PID2_WHEELS_SLOWDOWN_LIMIT);
        control.setSlowdownCoef(Settings.PID2_WHEELS_SLOWDOWN_KP);
        control.setAccel(Settings.PID2_WHEELS_ACCEL);
        this.sensorMotor = sMotor;
        /*
        sMotor.setSpeed(Settings.PID2_SENSOR_SPEED);
        sMotor.setAcceleration(Settings.PID2_SENSOR_ACCEL);
        */
        sensorMin = Settings.PID2_SENSOR_MIN;
        sensorMax = Settings.PID2_SENSOR_MAX;
        transferFilter = Filters.getNewFilter(Filters.getMode(Settings.PID2_TRANS_FILTER_TYPE),
                Settings.PID2_TRANS_FILTER_ARG1,
                Settings.PID2_TRANS_FILTER_ARG2,
                (int) Settings.PID2_TRANS_FILTER_ARG1,
                Settings.PID2_TRANS_FILTER_ARG1
        );
        //logger = new Logger();
    }

    /**
     * Sleep to create more consistent timing
     *
     * @param lastTimestamp Last timestamp
     */
    private static long sleep(long lastTimestamp, int millis) {
        long stamp = System.nanoTime();
        long wait = stamp - lastTimestamp;
        Delay.msDelay(millis - Math.round(wait / 1_000_000f));
        return stamp;
    }

    @Override
    public Thread getThread() {
        if (thread == null) {
            thread = new Thread(this);
            thread.setPriority(Thread.MAX_PRIORITY);
            thread.setDaemon(false);
            thread.setName("alternative destructor");
        }
        return thread;
    }

    @Override
    public void setStopFlag(boolean flag) {
        this.run = !flag;
        if (!run)
            getThread().interrupt();
    }

    @Override
    public void run() {
        control.push(0, 0);
        sensorMotor.forward();
        Thread currentThread = Thread.currentThread();
        long lastWaitTimestamp = System.nanoTime();
        long lastMeasureTimestamp = System.nanoTime();
        Log log = Log.getInstance();
        while (run && !currentThread.isInterrupted()) {
            long stamp = System.nanoTime();
            float diff = (stamp - lastMeasureTimestamp) / 1_000_000f; // milliseconds
            if (diff == 0)
                diff = 0.01f;
            lastMeasureTimestamp = stamp;
            int light = sensor.getRedFast();
            int azimuth = sensorMotor.getTachoCount();


            int lightError = Settings.PID_SENSOR_TARGET - light;
            int sensorPower = pushSensor(lightError, azimuth, diff);
            int target = Math.round(sensorPower * Settings.PID2_TARGET_COEF) + azimuth;
            int robot_ccw = calcWheels(target, lightError, diff);

            int azimuthErrorFiltered = Math.round(
                    errorFilter.filter(
                            Math.abs(target - Settings.SENSOR_CENTER_OFFSET)));
            control.push(robot_ccw, azimuthErrorFiltered);

            Log.Sample sample = new Log.Sample();
            sample.time = lastMeasureTimestamp;
            sample.azimuth = azimuth;
            sample.lightError = lightError;
            sample.targetAzimuth = target;
            sample.azimuthError = target - Settings.SENSOR_CENTER_OFFSET;
            sample.robot_ccw = robot_ccw;
            sample.s_p = pidSensor.getLastP();
            sample.s_d = pidSensor.getLastD();
            sample.m_p = pidWheels.getLastP();
            sample.m_d = pidWheels.getLastD();
            log.add(sample);

            lastWaitTimestamp = sleep(lastWaitTimestamp, Settings.PID2_SLEEP);
        }
        control.stop();
        log.write();
    }

    private int pushSensor(int lightError, int oldAzimuth, float dt) {
        int power = Math.round(pidSensor.process(lightError, dt));

        if (Main.TRACK_MODE == TrackMode.RIGHT) {
            power *= -1;
        }
        sensorMotor.setPower(power);
        return power;
        /*
        int destination_noclip = oldAzimuth + turn;
        int destination = Utils.limit(destination_noclip, sensorMin, sensorMax);
        sensorMotor.rotateTo(destination, true);
        return destination_noclip;
        */
    }

    private int calcWheels(int targetAzimuth, int lightError, float dt) {
        int error = targetAzimuth - Settings.SENSOR_CENTER_OFFSET;
        int overflow = getSensorOverflow(targetAzimuth);
        if (overflow != 0) {
            error += overflow * Settings.PID2_OVERFLOW_COEF;
        }
        if (Math.abs(lightError) > Settings.PID2_LIGHT_LIMIT) {

            if (lightError > 0) {
                if (Main.TRACK_MODE == TrackMode.RIGHT) {
                    error -= Math.abs(lightError - Settings.PID2_LIGHT_LIMIT) * Settings.PID2_LIGHT_COEF_BLACK;
                } else {
                    error += Math.abs(lightError - Settings.PID2_LIGHT_LIMIT) * Settings.PID2_LIGHT_COEF_BLACK;
                }
            } else {
                if (Main.TRACK_MODE == TrackMode.RIGHT) {
                    error += Math.abs(lightError - Settings.PID2_LIGHT_LIMIT) * Settings.PID2_LIGHT_COEF_WHITE;
                } else {
                    error -= Math.abs(lightError - Settings.PID2_LIGHT_LIMIT) * Settings.PID2_LIGHT_COEF_WHITE;
                }
            }
        }
        float error_pid = transferFilter.filter(error);
        float diff = pidWheels.process(error_pid, dt);
        return Math.round(diff);
    }

    private int getSensorOverflow(int destination) {
        int sensor_overflow = 0;
        if (destination > sensorMax) {
            sensor_overflow = destination - sensorMax;
        } else if (destination < sensorMin) {
            sensor_overflow = destination - sensorMin;
        }
        return sensor_overflow;
    }

    public void placeSensor() {
        sensorMotor.setPower(50);
        sensorMotor.backward();
        Delay.msDelay(500);
        sensorMotor.stop();
        sensorMotor.flt();
        Delay.msDelay(25);
        sensorMotor.resetTachoCount();
        sensorMotor.stop();
    }
    /*
    public void placeSensor() {
        sensorMotor.setStallThreshold(10, 10);
        sensorMotor.setSpeed(360);
        sensorMotor.setAcceleration(2880);
        sensorMotor.backward();
        while (!sensorMotor.isStalled())
            Delay.msDelay(1);
        sensorMotor.flt();
        sensorMotor.setStallThreshold(50, 1000); // default
        Delay.msDelay(25);
        sensorMotor.resetTachoCount();
        if (Main.TRACK_MODE == TrackMode.LEFT) {
            sensorMotor.rotateTo(sensorMax);
        } else {
            sensorMotor.rotateTo(sensorMin);
        }
        sensorMotor.setSpeed(Settings.PID2_SENSOR_SPEED);
        sensorMotor.setAcceleration(Settings.PID2_SENSOR_ACCEL);
    }*/
}
