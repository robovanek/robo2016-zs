package tux.confgen;

import tux.shared.utils.Settings;

import java.io.IOException;

/**
 * Configuration file generator
 */
public class Main {
    public static void main(String[] args) throws IOException {
        String input;
        String output;
        if (args.length == 0) {
            input = Settings.CONFIG;
            output = Settings.CONFIG + ".new";
        } else {
            if (args.length != 2) {
                System.err.println("Usage: java -jar config.jar <input> <output>");
                return;
            }
            input = args[0];
            output = args[1];
        }
        Settings.load(input);
        Settings.save(output);
    }
}
