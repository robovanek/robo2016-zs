package tux.rotator;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Keys;
import lejos.hardware.LED;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.BasicMotorPort;
import lejos.utility.Delay;
import tux.shared.utils.Settings;

import java.io.IOException;

/**
 * Prints out position of the sensor motor.
 */
public class Main implements Runnable {
    private UnregulatedMotor motor;
    private Keys keys;
    private LED led;

    public Main() {
        Brick brick = BrickFinder.getLocal();
        led = brick.getLED();
        led.setPattern(2);
        motor = new UnregulatedMotor(brick.getPort(Settings.SENSOR_MOTOR_PORT), BasicMotorPort.PWM_BRAKE);
        keys = brick.getKeys();
    }

    public static void main(String[] args) throws IOException {
        Settings.load(Settings.CONFIG);
        new Main().run();
    }

    @Override
    public void run() {
        led.setPattern(1);
        while ((keys.readButtons() & Keys.ID_ESCAPE) == 0) {
            System.out.println(motor.getTachoCount());
            Delay.msDelay(1);
        }
        led.setPattern(0);
    }
}
