package tux.pid.sensor;

import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
import tux.shared.utils.Settings;

import java.io.Closeable;
import java.io.IOException;

/**
 * Sensor lock
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.5
 * @deprecated Not usable anymore with current construction
 */
@Deprecated
public class SensorHolder implements Closeable {
    private RegulatedMotor motor;

    public SensorHolder(Port port) {
        this.motor = new EV3MediumRegulatedMotor(port);
    }

    public void placeSensor() {
        motor.setSpeed(90);
        motor.setStallThreshold(10, 10);
        motor.backward();
        while (!motor.isStalled())
            Delay.msDelay(1);
        motor.stop();
        motor.flt();
        Delay.msDelay(25);
        motor.resetTachoCount();
        motor.rotateTo(Settings.EFFECT_SENSOR_OFFSET);
    }

    @Override
    public void close() throws IOException {
        motor.close();
    }
}
