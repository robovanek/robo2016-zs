#!/bin/sh
cd "$(dirname "$(realpath "$0")")"
scp ../out/artifacts/ConfGen/config.jar \
	../out/artifacts/SharedLib/tardis-util.jar \
	../out/artifacts/Alternative/alternative.jar \
	root@10.42.0.33:/home/lejos/programs
