package tux.shared;

/**
 * Line tracking mode
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 1.0
 */
public enum TrackMode {
    /**
     * Track the line on it's left side.
     */
    LEFT,
    /**
     * Track the line on it's right side.
     */
    RIGHT
}
