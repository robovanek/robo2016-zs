package tux.shared.pid;

import tux.shared.pid.filter.Filter;

import java.util.Objects;

/**
 * PID controller maths
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.7
 */
public class PID {
    private float p;
    private float i;
    private float d;
    private float lastError;
    private float integral;
    private Filter filter;
    private float lastP;
    private float lastI;
    private float lastD;
    private float lastValue;

    public PID(float p, float i, float d, Filter derivationFilter) {
        this.p = p;
        this.i = i;
        this.d = d;
        this.lastError = Float.NaN;
        this.integral = 0;
        this.filter = Objects.requireNonNull(derivationFilter);
    }

    public float getLastP() {
        return lastP;
    }

    public float getLastI() {
        return lastI;
    }

    public float getLastD() {
        return lastD;
    }

    public float getLastValue() {
        return lastValue;
    }

    public float process(float error, float time) {
        if (time == 0)
            throw new IllegalArgumentException("Zero time is not allowed! If you want to ignore this feature, use one instead.");

        float derivation;
        if (lastError == Float.NaN) {
            derivation = 0;
        } else {
            derivation = filter.filter((error - lastError) / time);
        }
        lastError = error;
        integral += error * time;

        lastP = error * p;
        lastI = i * integral * p;
        lastD = d * derivation * p;
        return lastValue = lastP + lastI + lastD;
    }
}
