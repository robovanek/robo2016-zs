package tux.shared.pid.filter;

/**
 * Created by kuba on 5.4.16.
 */
public interface Filter {
    float filter(float input);
}
