package tux.shared.pid.filter;

/**
 * Filter factory class
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.7
 */
public final class Filters {
    private Filters() {
    }

    public static FilteringMode getMode(String filter) {
        switch (filter.toLowerCase()) {
            case "weighted average":
            case "average":
            case "avg":
                return FilteringMode.WEIGHTED_AVERAGE;
            case "median":
            case "med":
                return FilteringMode.MEDIAN;
            case "low pass":
            case "lowpass":
                return FilteringMode.LOW_PASS;

            case "none":
            default:
                return FilteringMode.NONE;

        }
    }

    public static Filter getNewFilter(FilteringMode mode, float avgActual, float avgHist, int medSize, float lowPass) {
        switch (mode) {
            case WEIGHTED_AVERAGE:
                return new WeightedAverageFilter(avgActual, avgHist);

            case MEDIAN:
                return new MedianFilter(medSize);

            case LOW_PASS:
                return new LowPassFilter(lowPass);

            case NONE:
            default:
                return new NoneFilter();

        }
    }

    public enum FilteringMode {
        WEIGHTED_AVERAGE,
        MEDIAN,
        LOW_PASS,
        NONE;
    }

    private static class NoneFilter implements Filter {
        @Override
        public float filter(float input) {
            return input;
        }
    }
}
