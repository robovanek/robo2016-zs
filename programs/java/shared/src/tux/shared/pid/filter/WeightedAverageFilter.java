package tux.shared.pid.filter;

/**
 * Created by kuba on 5.4.16.
 */
public class WeightedAverageFilter implements Filter {
    private final float historyCoef;
    private final float actualCoef;
    private float history = Float.NaN;

    public WeightedAverageFilter(float actualWeight, float historyWeight) {
        if (actualWeight == 0f || historyWeight == 0f)
            throw new IllegalArgumentException("Zero weight is not allowed.");
        actualCoef = actualWeight / (actualWeight + historyWeight);
        historyCoef = historyWeight / (actualWeight + historyWeight);
    }

    public float filter(float input) {
        if (Float.isNaN(history))
            return (history = input);
        return (history = input * actualCoef + history * historyCoef);
    }

    public void reset() {
        history = Float.NaN;
    }
}
