/**
 * Extension to the leJOS LED support
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
package tux.shared.led;