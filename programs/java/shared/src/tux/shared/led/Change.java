package tux.shared.led;

/**
 * Changes of the light state over time
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.2
 */
public enum Change {
    /**
     * The LEDs are still on.
     */
    STEADY(0),
    /**
     * The LEDs are blinking - periodically switched on and off for the same amount of time.
     */
    BLINK(3),
    /**
     * The LEDs are double-blinking - periodically switched on and off for different amounts of time
     */
    HEARTBEAT(6);
    private final int code;

    Change(int code) {
        this.code = code;
    }

    /**
     * Get code associated with this change type.
     *
     * @return Code, NOT additive
     */
    public int getCode() {
        return code;
    }
}
