package tux.shared;

import lejos.hardware.Keys;
import lejos.hardware.Sound;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.Port;
import lejos.robotics.RegulatedMotor;
import tux.shared.led.Change;
import tux.shared.led.Color;
import tux.shared.led.LED;
import tux.shared.sensor.HackedSensor;
import tux.shared.utils.Settings;

import java.io.Closeable;
import java.io.IOException;

/**
 * User interface
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 0.5
 */
public class UserMain {
    /**
     * LCD used for displaying
     */
    private final TextLCD lcd;
    /**
     * Keys used for input
     */
    private final Keys keys;
    private final String programTitle;
    private final lejos.hardware.LED led;

    public UserMain(TextLCD lcd, Keys keys, lejos.hardware.LED led, String programTitle) {
        this.led = led;
        this.programTitle = programTitle;
        this.keys = keys;
        this.lcd = lcd;
        lcd.setAutoRefresh(false);
    }

    public TrackMode getTrackMode() {
        LED.setPattern(led, Color.ORANGE, Change.STEADY);
        lcd.clear();
        lcd.drawString(programTitle, 4, 0, true);
        lcd.drawString("R/L tracking:", 0, 1);

        TrackMode result;
        final int base = 2;
        // names for option indexes
        final String[] options = {"right", "left"};
        // values for option indexes
        final TrackMode[] values = {TrackMode.RIGHT, TrackMode.LEFT};
        // selected index
        int selected = 0;
        // until user selects something
        for (; ; ) {
            // draw options
            for (int i = 0; i < options.length; i++) {
                lcd.drawString(options[i], 2, base + i, i == selected);
            }
            lcd.refresh();
            // wait for any press
            int mask = keys.waitForAnyPress();
            if ((mask & Keys.ID_ENTER) != 0) {
                // user confirmed selection, clear lcd and return
                lcd.clear(1);
                lcd.clear(2);
                lcd.clear(3);
                lcd.refresh();
                result = values[selected];
                break;
            } else if ((mask & Keys.ID_UP) != 0) {
                // user changed the selection
                if (selected > 0)
                    selected--;
                else
                    selected = options.length - 1;
            } else if ((mask & Keys.ID_DOWN) != 0) {
                // user changed the selection
                if (selected < options.length - 1)
                    selected++;
                else
                    selected = 0;
            } else if ((mask & Keys.ID_ESCAPE) != 0) {
                // user wants to exit the program
                lcd.clear();
                lcd.refresh();
                LED.turnOff(led);
                return null;
            }
        }
        LED.setPattern(led, Color.ORANGE, Change.BLINK);
        return result;
    }

    public boolean getStart() {
        // beep
        new Thread(new Runnable() {
            @Override
            public void run() {
                Sound.beep();
            }
        }).start();
        lcd.drawString(programTitle, 4, 0, true);
        // draw instructions on the screen
        lcd.drawString("ENTER ->", 0, 1, false);
        lcd.drawString(">TARDIS<", 7, 2, true);
        lcd.drawString("ESCAPE -> exit", 0, 3, false);
        lcd.refresh();
        // until user does valid action
        for (; ; ) {
            // wait for any press
            int id = keys.waitForAnyPress();
            if ((id & Keys.ID_ENTER) != 0) {
                // user confirmed
                lcd.clear(1);
                lcd.clear(2);
                lcd.clear(3);
                lcd.refresh();
                LED.setPattern(led, Color.GREEN, Change.BLINK);
                return true;
            }
            if ((id & Keys.ID_ESCAPE) != 0) {
                // user declined
                lcd.clear();
                lcd.refresh();
                LED.turnOff(led);
                return false;
            }
        }
    }

    /**
     * Safe loop for creating large motor
     *
     * @param port Port for motor
     * @return Initialized motor
     */
    public RegulatedMotor loadLargeMotor(Port port) {
        RegulatedMotor m = null;
        boolean success = false;
        while (!success) {
            try {
                m = new EV3LargeRegulatedMotor(port);
                success = true;
            } catch (Exception ignored) {
                Sound.playTone(400, 250);
            }
        }
        return m;
    }

    /**
     * Safe loop for creating medium motor
     *
     * @param port Port for motor
     * @return Initialized motor
     */
    public RegulatedMotor loadMediumMotor(Port port) {
        RegulatedMotor m = null;
        boolean success = false;
        while (!success) {
            try {
                m = new EV3MediumRegulatedMotor(port);
                success = true;
            } catch (Exception ignored) {
                Sound.playTone(400, 250);
            }
        }
        return m;
    }

    /**
     * Safe loop for creating unregulated motor
     *
     * @param port Port for motor
     * @return Initialized motor
     */
    public UnregulatedMotor loadUnregulatedMotor(Port port) {
        UnregulatedMotor m = null;
        boolean success = false;
        while (!success) {
            try {
                m = new UnregulatedMotor(port);
                success = true;
            } catch (Exception ignored) {
                Sound.playTone(400, 250);
            }
        }
        return m;
    }

    /**
     * Wait for exit
     */
    public void waitForEnd() {
        for (; ; ) {
            int val = keys.waitForAnyPress();
            if ((val & Keys.ID_ESCAPE) != 0) {
                return;
            }
        }
    }

    /**
     * Load settings from file {@link Settings#CONFIG}
     */
    public void loadSettings() {
        try {
            Settings.load(Settings.CONFIG);
        } catch (IOException e) {
            System.err.println("FATAL ERROR");
            throw new RuntimeException("Cannot load Settings", e);
        }
    }

    public void tryClose(Thread thread, Closeable... closeable) {
        if (thread != null) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (Closeable var : closeable) {
            try {
                var.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Safe RegulatedMotorloop for creating color sensor instance
     *
     * @param sensor Port for sensor
     * @return Initialized color sensor
     */
    public HackedSensor loadSensor(Port sensor) {
        HackedSensor s = null;
        boolean success = false;
        while (!success) {
            try {
                s = new HackedSensor(sensor);
                success = true;
            } catch (Exception ignored) {
                Sound.playTone(800, 250);
            }
        }
        return s;
    }
}
